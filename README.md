# **Realtime Chat** #

Bisa untuk bikin aplikasi realtime fetch data.
Sesuai request kemarin, kali ini saya share code sederhana simple chat multi room, bisa chat user to user di private room, dan ada notifikasi. Saya bukan ahlinya dibidang ini, jadi mafkan jika masih jauh dari powerfull codingan saya, tapi bagi pemula yang mau belajar socket, inshaAllah ini bermanfaat.

* Ini chat server nya : http://pastebin.com/rxNy0Cn0 (app.js)
* Ini view page nya : http://pastebin.com/rWQzUtgd (index.html)
* Wajib install nodejs+npm, library yang dibutuhkan ada disini http://pastebin.com/nde7i9U1 (package.json)

- Tinggal jadikan 1 folder
- npm install
- ganti ip socket di index.html ke ip anda atau bisa localhost
- bikin database chat_sql - table chat (atau jalankan query ini http://pastebin.com/GP4bcFAw)
- jalankan dengan command node app.js atau mau lebih mudah pake nodemon, auto debug ketika codingan selesai dirubah.

Demo ada disini http://handokoaji.cf/chat/

Masih ada beberapa bug, diantaranya jika ada nama user yang sama, mungkin akan bentrok, kemudian belum berjalan normal di firefox(bugs firefox), dan user yang terakhir join, tidak bisa melihat user online sebelum ada user lagi yang connect ke socket. Untuk mencoba gunakan username camelCase atau dengan tanda underline atau dash. Jangan menggunakan spasi. Untuk mencoba dengan 2 user, gunakan chrome biasa dan tab incognito chrome, karena jika hanya beda tab, chat tidak berjalan.

Logic nya sederhana, socket.io bisa digunakan untuk broadcast ke suatu room, dan semua user di room tersebut akan mendapatkan message broadcast. Nah bagaimana untuk private chat only 1 user to user ?
Saya cuman bikin multi room, dimana terdapat room default, saya beri nama room "notif" dan room dinamis yang bisa dibuat kapan saja.

Skenario pertama semua user yang ingin chat harus connect dulu ke socket, dan otomatis akan berada di room notif. Di room ini user bisa melakukan chatting ke semua user, karena sifatnya broadcast di room notif. Kemudian jika ingin chat ke spesifik user, misal user A ingin chat ke user B. User A leave room notif kemudian join room baru, misalkan room A_B karena room tsb nantinya hanya akan berisi user A dan B. Room ini harus unique. Oleh karena itu gunakan saja kedua nama user untuk create room. Mungkin bug nya ketika ada nama user yang sama. Jika akan membuat sistem yang kompleks, silahkan gunakan user_id, karena user_id biasanya unique.

Setelah user A join room A_B, kemudian user A melakukan chat di room tersebut. Kondisi room hanya ada user A, karena itu diperlukan parameter tambahan yakni nama user B untuk mengirim notif ke user B. Sederhananya seperti ini:


```
#!javascript

socket.emit('joinroom', function(newroom, nama_user_B)
```


Kemudian di server cukup dengan:

```
#!javascript

socket.on('joinroom', function(newroom, nama_user_B)
```


Oh ya, didalam socket.io command ON berfungsi untuk menangkap perintah/statement/kondisi/data sedangkan Emit berfungsi untuk mengirim perintah/data/statement.

Setelah user A melakukan chat, untuk mengirim notifikasi ke user B, cara sederhananya adalah dengan mengecek isi dari room tersebut. Jika room memiliki kurang dari 2 user, emit message ke default room (notif) untuk user B. Disinilah parameter nama_user_B digunakan. Karena hanya user yang memiliki nama dari parameter tersebutlah yang akan mendapat message. Nah data message tadi digunakan sebagai notifikasi, bahwa user B memiliki pesan baru di room A_B. Di dalam data notif yang dikirimkan, harus terdapat nama user A. Karena skenario nya akan kembali ke awal lagi, ketika user B ingin membalas chat, user B harus leave room notif dan join room A_B.

Code nya kurang lebih seperti ini :

```
#!javascript

io.sockets.in(defaultroom).emit(nama_user_B, {nama_user:socket.nama_user, message: message});
```


Untuk cek jumlah user di room :

```
#!javascript

io.sockets.adapter.rooms[namaroom].length > 1
```


Terakhir, user B tinggal join room dengan kombinasi nama user A dan user B sendiri. Untuk history chat, atau misal user B sedang tidak di room notif/disconnect, karena chat disimpan di DB, berikan tanda read/unread ke data chat. Jika room A_B hanya terdapat 1 user, data chat unread else read. Ketika user B connect dan join room notif, ambil data dari db yang masih unread untuk user B, emit ke user B. Kemudian lakukan pengecekan jika user B masuk ke room A_B, update semua data chat untuk user B menjadi read.

Bagi yang mau mengembangkan chat ini monggo, syukur2 bisa bug fix yang masih ada di chat ini agar bisa bermanfaat untuk yang lain. Sekian, semoga bermanfaat. cmiiw...