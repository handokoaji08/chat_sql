var express = require('express');
var path = require('path');
var app = express();
var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);
// var moment = require('moment');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
server.listen(5000, '0.0.0.0', function () {
	console.log('Server listening at port %d', 5000);
});
var connection = require('express-myconnection'),
    mysql = require('mysql');

var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'chat_sql',
    password: '',
    debug: false
});
app.use(function(req, res, next) {
  res.send('Server Chat is Running!');
});

var users = {};
var numUsers = 0;
var rooms = {};
var defaultroom = 'notif';
io.on('connection', function (socket){
	var addedUser = false;
	socket.broadcast.emit('listuser', {
			nama_user: users
		});
	socket.on('adduser', function (nama_user){
		socket.nama_user = nama_user;
		users = nama_user;
		++numUsers;
		addedUser = true;
		socket.room = defaultroom;
		rooms[defaultroom] = defaultroom;
		socket.join(defaultroom);
		console.log(users);
		socket.broadcast.emit('userjoined', {
			nama_user: socket.nama_user,
			numUsers: numUsers
		});
		socket.broadcast.emit('listuser', {
			nama_user: users
		});
		//notif chat belum dibaca
		db.query('SELECT id_chat, user_from, user_to, room, message, dibaca from chat WHERE dibaca ="0" and user_to ="' +socket.nama_user+ '"', function(err, chatdata){
			if (err) return console.log(err);
			for (var i = 0; i < chatdata.length; i++) {
						var notifbaru = {"user_to": chatdata[i].user_to, "id_chat": chatdata[i].id_chat, "user_from": chatdata[i].user_from, "message": chatdata[i].message, "room": chatdata[i].room, "dibaca": chatdata[i].dibaca};
						socket.emit(chatdata[i].user_to, notifbaru);
				};
			
			});
		
		
	});
	socket.broadcast.emit('listuser', {
			nama_user: users
		});
	socket.on('switchRoom', function (newroom, user_to){
		socket.leave(socket.room);
		rooms[newroom] = newroom;
		socket.join(newroom);
		socket.room = newroom;
		socket.user_to = user_to;
		// console.log(socket.room);
		socket.broadcast.emit('listuser', {
			nama_user: users
		});
		db.query('SELECT id_chat, user_from, user_to, room, message from chat WHERE room ="' +socket.room+ '"', function(err, chatdata){
			if (err) return console.log(err);
			for (var i = 0; i < chatdata.length; i++) {
				
					socket.emit('historychat', {
						id_chat: chatdata[i].id_chat,
						user_from: chatdata[i].user_from,
						user_to: chatdata[i].user_to,
						room: chatdata[i].room,
						message: chatdata[i].message
					});
				
			}
		});

		db.query('SELECT room from chat WHERE room ="' +socket.room+ '" and user_to ="'+socket.nama_user+'"', function(err, readdata){
				if (err) return console.log(err);
				for (var i = 0; i < readdata.length; i++) {
					db.query('UPDATE chat set dibaca = "1" WHERE room ="' +readdata[i].room+ '" and user_to ="'+socket.nama_user+'"');
				};
			});

		// db.query('SELECT id_rooms, pembeli_delete, penjual_delete from rooms WHERE room ="'+socket.room+'"', function(err, logdata){
		// 	if (err) return console.log(err);
		// 	for (var i = 0; i < logdata.length; i++) {
		// 		if (logdata[i].pembeli_delete == '1') {
		// 			db.query('UPDATE rooms set pembeli_delete = "0" WHERE id_rooms ="'+logdata[i].id_rooms+'"');
		// 		}else if (logdata[i].penjual_delete == '1') {
		// 			db.query('UPDATE rooms set penjual_delete = "0" WHERE id_rooms ="'+logdata[i].id_rooms+'"');
		// 		};
		// 	};
		// });

	});

	socket.on('backToDefaultRoom', function (nama_user){
		socket.leave(socket.room);
		socket.join(defaultroom);
		socket.room = defaultroom;
		socket.nama_user = nama_user;
		
		socket.broadcast.emit('listuser', {
			nama_user: users
		});
		
		db.query('SELECT id_chat, user_from, user_to, room, message, dibaca from chat WHERE dibaca ="0" and user_to ="' +socket.nama_user+ '"', function(err, chatdata){
		if (err) return console.log(err);
		for (var i = 0; i < chatdata.length; i++) {
				var notifbaru = {"user_to": chatdata[i].user_to, "id_chat": chatdata[i].id_chat, "user_from": chatdata[i].user_from, "message": chatdata[i].message, "room": chatdata[i].room, "dibaca": chatdata[i].dibaca};
				socket.emit(chatdata[i].user_to, notifbaru);
			};
		
		});
		

	});
	
	socket.on('newmessage', function (message){
		var cekroom = io.sockets.adapter.rooms[socket.room];
		//cek apakah room lebih dari 1 user ?
		// console.log(cekroom.length);
		if (cekroom.length > 1) {
			//untuk read semua chat unread
			db.query('SELECT room from chat WHERE room ="' +socket.room+ '" and user_to ="'+socket.nama_user+'"', function(err, readdata){
				if (err) return console.log(err);
					for (var i = 0; i < readdata.length; i++) {
						db.query('UPDATE chat set dibaca = "1" WHERE room ="' +readdata[i].room+ '" and user_to ="'+socket.nama_user+'"');
				};
			});
		

			db.query('INSERT INTO chat (user_from, user_to, room, message, dibaca) VALUES ("' + socket.nama_user + '", "' +socket.user_to+ '", "' +socket.room+ '", "' +message+ '", "1")', function(err, result){
				if (err) throw err;
				io.sockets.in(socket.room).emit('newmessage', {
					id_chat: result.insertId,
					nama_user: socket.nama_user,
					message: message
				});
			});
			socket.broadcast.emit('listuser', {
				nama_user: users
			});

		}else{
			
			db.query('INSERT INTO chat (user_from, user_to, room, message, dibaca) VALUES ("' + socket.nama_user + '", "' +socket.user_to+ '", "' +socket.room+ '", "' +message+ '", "0")', function(err, result){
				if (err) throw err;
				io.sockets.in(socket.room).emit('newmessage', {
					id_chat: result.insertId,
					nama_user: socket.nama_user,
					message: message
				});
			});
			var notifuser = {"user_to": socket.user_to, "user_from": socket.nama_user, "message": message, "room": socket.room, "dibaca": "0"};
			io.sockets.in(defaultroom).emit(socket.user_to, notifuser);
			socket.broadcast.emit('listuser', {
				nama_user: users
			});		
		}
});



	socket.on('disconnect', function (){
		if (addedUser) {
			delete users[socket.nama_user];
			--numUsers;

			socket.broadcast.emit('userleft', {
				id_user: socket.nama_user,
				numUsers: numUsers
			});
		}
	});

});